import unittest

def addThree(a, b, c):
    return a + b + c

def subtractThree(a, b, c):
    return a - b - c

def multiplyThree(a, b, c):
    return a * b * c

def divideThree(a, b, c):
    return a / b / c

class TestCalculator(unittest.TestCase):

    def test_addThree(self):
        self.assertEqual(addThree(1, 2, 3), 1 + 2 + 3)
        
    def test_subtractThree(self):
        self.assertEqual(subtractThree(1, 2, 3), 1 - 2 - 3)
        
    def test_multiplyThree(self):
        self.assertEqual(multiplyThree(1, 2, 3), 1 * 2 * 3)
        
    def test_divideThree(self):
        self.assertAlmostEqual(divideThree(1, 2, 3), 1 / 2 / 3, places = 6, delta = 0.00001)

    
if __name__ == '__main__':
    unittest.main()
